(function() {

    // 取得設定資料
    const configs = require('../configs.js');

    // 引用寫好的API
    const assistantAPI = require('../services/AssistantAPIV2');
    assistantAPI.init(configs.assistant)

    // 存放 sdk 產生之 session
    let userSessions = {};

    class AssistantController {
        constructor() {
            this.sendMessage = this.sendMessage.bind(this);
        }
        
        // 取得 user session 
        async sendMessage(userId, text) {

			// 取出API所需暫存Session
			let userSession = userSessions.userId;

            // 若沒有對應session
            if(!userSession){
				// create new session
				let newSession = await assistantAPI.createSession();
				userSessions[userId] = userSession;
				userSession = newSession.session_id;
            }
            
            // 對話
            return assistantAPI.message(text, userSession)
            .then((result) => {
                console.log('call Assistant api success');

                // 整理過Assistant後，僅需要作
                return result;
            }).catch((err) => {
                return 'fail to call assistant';
            });
        }
    }

    module.exports = new AssistantController();
}());