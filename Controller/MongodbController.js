//取得設定資料
const configs = require('../configs.js');

const MongoClient = require('mongodb').MongoClient;
const uri = configs.mongodb;

class MongodbController{
    constructor(){
        this.init = this.init.bind(this);
        this.insertOne =this.insertOne.bind(this);
    }

    init(){
        let _this = this;
        return new Promise(resolve=>{
        MongoClient.connect(uri, {useNewUrlParser:true, useUnifiedTopology:true}, function(err, db){
            if (err){
                console.log('error', err);
            }
            else{
                console.log('success to db');
                _this.db = db;
            }

            return err || 'success';
        })
    })
    }

    insertOne(dbName, collection, document){
        return new Promise(resolve=>{
        let dbo = this.db.db(dbName);
        dbo.collection(collection).insertOne(document, function(err, res){
            if(err){
                console.log('error', err);
            }
            else{
                console.log('success to document');
            }
            resolve(err || 'success');
        })
    })
    }
}
module.exports = new MongodbController();