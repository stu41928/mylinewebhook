let express = require('express');
let router = express.Router();

const AssistantCtl = require('../Controller/AssistantController');
const MongodbCtl = require('../Controller/MongodbController');
MongodbCtl.init();
const logger = require('../Controller/Logger');
logger.init('webhook');

const line = require('@line/bot-sdk');

const configs = require('../configs.js');

const channelAccessToken = configs.channelAccessToken;

const lineClient = new line.Client({
	channelAccessToken: channelAccessToken
});

const SimpleNodeLogger = require('simple-node-logger');

router.post('/line', async function(req, res, next) {

	logger.log('webhook received an event');
    res.status(200).send();
	
	let targetEvent = req.body.events[0];
	if(targetEvent.type == 'message'){
		
		if(targetEvent.message.type == 'text'){

			let userId = targetEvent.source.userId,
				userSay = targetEvent.message.text;

			logger.log(userId, 'says:', userSay);

			let AssistantReturn = await AssistantCtl.sendMessage(userId, userSay);
			let returnMessage = {
				type: 'text',
				text: AssistantReturn.output.generic[0].text
			};
			
			replyToLine(targetEvent.replyToken, returnMessage);
			logger.log(userId, 'return message:', returnMessage);

			let document = {
				timestamp: Date.now(),
				userId: userId,
				userSay: userSay,
				AssistantReturn: AssistantReturn
			}
			MongodbCtl.insertOne('LineBot', 'chatLog', document);
		}
	}
});

module.exports = router;

function replyToLine(rplyToken, message) {

	return lineClient.replyMessage(rplyToken, message)
	.then((res) => {
		// console.log(res)
		return true; 
	})
	.catch((err) => {
		// console.log(err)
		return false;
	});
}